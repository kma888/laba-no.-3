﻿#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include "tree_elem.hpp"
#include "binary_tree.hpp"


template<typename T>
void pow2(tree_elem<T>* node){
    node->m_data = node->m_data * node->m_data;
}

template<typename T>
bool is_more_than_x(tree_elem<T>* node){
    if(node->m_data >= 10)
        return true;
    return false;
}

template<typename T>
T sum(tree_elem<T>* node,T node_sum){
    return node_sum + node->m_data;
}

void work_test(){

    int size = 10000;
    unsigned int start_time =  clock(); // начальное время
    binary_tree<int> first1;
    for (int i = 0; i < size; ++i)
    {
        first1.insert(rand()%size);
    }
    first1.Map(pow2);
    unsigned int end_time = clock(); // конечное время
    unsigned int search_time = end_time - start_time; // искомое время
    std::cout << "Time for " << size << " elements is " << search_time << " mls" << std::endl;

    size = 100000;
    start_time =  clock(); // начальное время
    binary_tree<int> first2;
    for (int i = 0; i < size; ++i)
    {
        first2.insert(rand()%size);
    }
    first2.Map(pow2);
    end_time = clock(); // конечное время
    search_time = end_time - start_time; // искомое время
    std::cout << "Time for " << size << " elements is " << search_time << " mls" << std::endl;
}

void BalanceTestCase(){

    char a;
    std::cout << " a)Balance your own tree b)create a random one c)exit. Type: a/b/e:     ";
    std::cin >> a;

    switch (a) {
        case 'a': {
            int n;
            std::cout << "Enter count of values in your tree :\n";
            std::cin >> n;
            int *values = new int[n];
            int value;
            std::cout << "Enter " << n << " elements for your tree :\n";
            for (int i = 0; i < n; ++i) {
                if (std::cin >> value)
                    values[i] = value;
                else {
                    delete[] values;
                    std::cout << "Invalid input\n";
                }
            }
            binary_tree<int> first;
            for (int i = 0; i < n; ++i) {
                first.insert(values[i]);
            }
            std::cout << "First tree :\n";
            first.print();
            //===========================================================//
            std::cout << std::endl;
            std::cout << "Balancing a tree:\n";

            first.balance_tree()->print();

            BalanceTestCase();
        }

        case 'b': {
            int size;
            std::cout << "Enter the size of new tree: ";
            std::cin >> size;

            binary_tree<int> tree;
            while (tree.get_size() < size) {
                tree.insert(rand() % (2*size) + 1);
            }

            tree.print();
            //===========================================================//
            std::cout << std::endl;
            std::cout << "Balancing a tree:\n";

            tree.balance_tree()->print();

            BalanceTestCase();
        }

        default: { exit(0); }
    }
}

int main(){

    work_test();

    int n;
    std::cout << "Enter count of values in your first tree :\n";
    std::cin >> n;

    int* values = new int [n];
    int value;
    std::cout << "Enter "<< n <<" elements for your first tree :\n";
    for (int i = 0; i < n; ++i)
    {	if(std::cin >> value)
            values[i] = value;
        else{
            delete[] values;
            std::cout << "Invalid input\n";
            return -1;
        }
    }
    binary_tree<int> first;
    for (int i = 0; i < n; ++i)
    {
        first.insert(values[i]);
    }
    std::cout << "First tree :\n";
    first.print();
    //===========================================================//
    std::cout << "Enter count of values in your second tree :\n";
    if(!(std::cin >> n)){
        delete[] values;
        std::cout << "Invalid input\n";
        return -1;
    }
    int* another_values = new int [n];
    std::cout << "Enter "<< n <<" elements for your second tree :\n";
    for (int i = 0; i < n; ++i)
    {	if(std::cin >> value)
            another_values[i] = value;
        else{
            delete[] values;
            delete[] another_values;
            std::cout << "Invalid input\n";
            return -1;
        }
    }
    binary_tree<int> second;
    for (int i = 0; i < n; ++i)
    {
        second.insert(another_values[i]);
    }
    std::cout << "Second tree :\n";
    second.print();
    //===========================================================//
    if(first.is_subtree(&first,&second))
        std::cout << "Second tree is subtree of first tree !\n";
    else
        std::cout << "Second tree is not subtree of first tree !\n";
    first.Map(pow2);
    std::cout << "First tree after squaring each element :\n";
    first.print();
    second.Where(is_more_than_x);
    std::cout << "All items less than 10 that belong to the second tree :\n";
    second.print();
    std::cout << "The sum of the elements of the modified first tree = " << first.Reduce(sum) << std::endl;
    delete[] values;
    delete[] another_values;
    //===========================================================//

    std::cout << "The union of the first and the second tree: \n";
    first.join_trees(&second);
    first.print();

    std::cout << "Get the subtree of the tree by key: \n";
    std::cout << "Enter key: \n";
    int key;
    std::cin >> key;
    first.get_sub_tree(key)->print();

    std::string traversal;
    std::cout << "Get the values of tree as string\n";
    std::cout << "Enter the type of traversal (for example {k}(L)[p]; k – root, l – left, r – rigth):\n";
    std::cin >> traversal;
    int var = 0;
    for (int i=0; i<sizeof (traversal); ++i){
        if (traversal[i]=='l'||traversal[i]=='L') {var = var*10 + 2;}
        if (traversal[i]=='k'||traversal[i]=='K') {var = var*10 + 1;}
        if (traversal[i]=='p'||traversal[i]=='P') {var = var*10 + 3;}
    }
    switch (var) {
        case 123:{
            first.tree_to_sring(1);
            break;}
        case 132:{
            first.tree_to_sring(2);
            break;}
        case 213:{
            first.tree_to_sring(3);
            break;}
        case 231:{
            first.tree_to_sring(4);
            break;}
        case 312:{
            first.tree_to_sring(5);
            break;}
        case 321:{
            first.tree_to_sring(6);
            break;}
    }

    BalanceTestCase();

    return 0;
}