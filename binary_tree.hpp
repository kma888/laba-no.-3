#pragma once
#include <iomanip>
template<typename T>
class binary_tree{
private:

    tree_elem<T>* m_root;

    int size;

    void map_node(void(*func)(tree_elem<T>*),tree_elem<T>* node){
        if(node){
            map_node(func ,node->m_left);
            map_node(func,node->m_right);
            func(node);
        }
    }

    void where_node(bool(*func)(tree_elem<T>*),tree_elem<T>* node){
        if(node){
            where_node(func,node->m_left);
            where_node(func,node->m_right);

            if(func(node)){
                delete_elem(node->m_data);
            }
        }
    }

    void tree_to_string_help_KLP(tree_elem<T>* node,std::vector<std::string>& node_str){
        if(node){
            node_str.push_back(std::to_string(node->m_data));
            tree_to_string_help_KLP(node->m_left,node_str);
            tree_to_string_help_KLP(node->m_right,node_str);
        }
    }

    void tree_to_string_help_KPL(tree_elem<T>* node,std::vector<std::string>& node_str){
        if(node){
            node_str.push_back(std::to_string(node->m_data));
            tree_to_string_help_KPL(node->m_left,node_str);
            tree_to_string_help_KPL(node->m_right,node_str);
        }
    }

    void tree_to_string_help_LKP(tree_elem<T>* node,std::vector<std::string>& node_str){
        if(node){
            tree_to_string_help_KLP(node->m_left,node_str);
            node_str.push_back(std::to_string(node->m_data));
            tree_to_string_help_KLP(node->m_right,node_str);
        }
    }

    void tree_to_string_help_PKL(tree_elem<T>* node,std::vector<std::string>& node_str){
        if(node){
            tree_to_string_help_KLP(node->m_right,node_str);
            node_str.push_back(std::to_string(node->m_data));
            tree_to_string_help_KLP(node->m_left,node_str);
        }
    }

    void tree_to_string_help_LPK(tree_elem<T>* node,std::vector<std::string>& node_str){
        if(node){
            tree_to_string_help_LPK(node->m_left,node_str);
            tree_to_string_help_LPK(node->m_right,node_str);
            node_str.push_back(std::to_string(node->m_data));
        }
    }

    void tree_to_string_help_PLK(tree_elem<T>* node,std::vector<std::string>& node_str){
        if(node){
            tree_to_string_help_PLK(node->m_left,node_str);
            tree_to_string_help_PLK(node->m_right,node_str);
            node_str.push_back(std::to_string(node->m_data));
        }
    }

    void tree_to_vector_help(tree_elem<T>* node, std::vector<T>& node_str){
        if (node){
            tree_to_vector_help(node->m_left, node_str);
            node_str.push_back(node->m_data);
            tree_to_vector_help(node->m_right, node_str);
        }
    }

    void Reduce_help(T(*func)(tree_elem<T>*,T),tree_elem<T>* node,T& node_sum){
        if(node){
            Reduce_help(func,node->m_left,node_sum);
            node_sum = func(node,node_sum);
            Reduce_help(func,node->m_right,node_sum);
        }
    }

    void upgrade_sub_tree(binary_tree<T>* result,tree_elem<T>* node){
        if(node){
            upgrade_sub_tree(result,node->m_left);
            result->insert(node->m_data);
            upgrade_sub_tree(result,node->m_right);
        }
    }

    void printSubtree(tree_elem<T>* root, const std::string& prefix)
    {
        if (root == NULL) { return; }

        bool hasLeft = (root->m_left != NULL);
        bool hasRight = (root->m_right != NULL);

        if (!hasLeft && !hasRight) { return; }

        std::cout << prefix;
        std::cout << ((hasLeft  && hasRight) ? "├── " : "");
        std::cout << ((!hasLeft && hasRight) ? "└── " : "");

        if (hasRight)
        {
            bool printStrand = (hasLeft && hasRight && (root->m_right->m_right != NULL || root->m_right->m_left != NULL));
            std::string newPrefix = prefix + (printStrand ? "│   " : "    ");
            std::cout << root->m_right->m_data << std::endl;
            printSubtree(root->m_right, newPrefix);
        }

        if (hasLeft)
        {
            std::cout << (hasRight ? prefix : "") << "└── " << root->m_left->m_data << std::endl;
            printSubtree(root->m_left, prefix + "    ");
        }
    }

    void balance(binary_tree<T>* tree, std::vector<T> nodes){
        tree->insert(nodes[nodes.size()/2]);
        if (nodes.size()>1) {
            std::vector<T> sub1(&nodes[0], &nodes[nodes.size() / 2]);
            std::vector<T> sub2(&nodes[nodes.size() / 2], &nodes[nodes.size()]);
            balance(tree, sub1);
            balance(tree, sub2);
        }
    }

    void thread(tree_elem<T>* node, tree_elem<T>* L, tree_elem<T>* R){
        if (node->m_left){
            thread(node->m_left, L, node);
        }
        if (node->m_right){
            thread(node->m_right, node, R);
        }
        if (!node->m_left){
            node->m_left = L;
        }
        if (!node->m_right){
            node->m_right = R;
        }
        return;
    }

    tree_elem<T>* find_node_help(int key){
        tree_elem<T>* node = m_root;
        while(node && node->m_data != key){
            if(node->m_data > key)
                node = node->m_left;
            else
                node = node->m_right;
        }
        return node;
    }

public:
 
    binary_tree(){
        m_root = nullptr;
        size = 0;
    }

    binary_tree(int key){
        m_root = new tree_elem<T>(key);
        size = 1;
    }

    ~binary_tree(){
        delete_tree(m_root);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    int get_size() {return size;}

    binary_tree<T>* get_sub_tree(T key){
        tree_elem<T>* node = m_root;
        while(node && node->m_data!=key)
            if(node->m_data < key)
                node = node->m_right;
            else
                node = node->m_left;
        binary_tree<T>* result = new binary_tree<T>(node->m_data);
        upgrade_sub_tree(result,node);

        return result;
    }

    binary_tree<T>* get_sub_tree_by_node(tree_elem<T>* node){
        if (!node) {
            binary_tree<T>* new_tree = new binary_tree<T>;
            return new_tree;
        }
        else {
            binary_tree<T>* new_tree = new binary_tree<T>(node->m_data);
            return new_tree;
        }
    }

    binary_tree<T>* balance_tree(){
        auto new_tree = new binary_tree<T>;
        std::vector<T> nodes = tree_to_vector();
        new_tree->balance(new_tree, nodes);
        return new_tree;
    }

    void join_trees(binary_tree<T>* tree) {
        auto tmp = tree->m_root;
        if(tmp) {
            insert(tmp->m_data);
            if (tmp->m_left) {
                join_trees(tree->get_sub_tree_by_node(tree->m_root->m_left));
            }
            if (tmp->m_right) {
                join_trees(tree->get_sub_tree_by_node(tree->m_root->m_right));
            }
        }
    }

    void get_root(){
        std::cout << "Root is:  " << m_root->m_data << std::endl;
    }

    void print()
    {
        if (m_root == NULL) { return; }

        std::cout << m_root->m_data << std::endl;
        printSubtree(m_root, "");
        std::cout << std::endl;
    }

    void delete_tree(tree_elem<T>* node){
        if(node){
            delete_tree(node->m_left);
            delete[] node;
            delete_tree(node->m_right);
        }
    }

    bool find(int key){
        tree_elem<T>* node = m_root;
        while(node && node->m_data != key){
            if(node->m_data > key)
                node = node->m_left;
            else
                node = node->m_right;
        }
        if ((bool)node) { std::cout << "I found!!!"; }
        return (bool)node;
    }

    void insert(int key){
        if(m_root == nullptr){
            m_root = new tree_elem<T>(key);
            size = 1;
        }else{
            tree_elem<T>* node = m_root;
            while(node && node->m_data != key){
                if(node->m_data > key){
                    if(node->m_left)
                        node = node->m_left;
                    else{
                        node->m_left = new tree_elem<T>(key);
                        size++;
                        return;
                    }
                }
                else{
                    if(node->m_right)
                        node = node->m_right;
                    else{
                        node->m_right = new tree_elem<T>(key);
                        size++;
                        return;
                    }
                }
            }
        }
    }

    void delete_elem(T key){
        tree_elem<T>* node = m_root;
        tree_elem<T>* prev = m_root;
        while (node && node->m_data != key) {
            prev = node;
            if (node->m_data > key) {
                node = node->m_left;
            } else
                node = node->m_right;
        }
        if (!node)
            return;
        if (m_root->m_data==key){
            if (m_root->m_left){
                m_root = m_root->m_left;
                auto Ltree = get_sub_tree_by_node(m_root->m_right);
                join_trees(Ltree);
            }
            else if (m_root->m_left){
                m_root = m_root->m_right;
                auto Rtree = get_sub_tree_by_node(m_root->m_left);
                join_trees(Rtree);
            }
            delete[] prev;
        }


        if (prev->m_right == node) {
            if (node->m_right == nullptr && node->m_left == nullptr) {
                prev->m_right = nullptr;
                delete[] node;
                size--;
                return;
            }
            if (node->m_left == nullptr) {
                prev->m_right = node->m_right;
                delete[] node;
                size--;
                return;
            }
            if (node->m_right == nullptr) {
                prev->m_right = node->m_left;
                delete[] node;
                size--;
                return;
            }
        }
        if (prev->m_left == node) {
            if (node->m_right == nullptr && node->m_left == nullptr) {
                prev->m_left = nullptr;
                delete[] node;
                size--;
                return;
            }
            if (node->m_left == nullptr) {
                prev->m_left = node->m_right;
                delete[] node;
                size--;
                return;
            }
            if (node->m_right == nullptr) {
                prev->m_left = node->m_left;
                delete[] node;
                size--;
                return;
            }
        }

        tree_elem<T> *tmp = node->m_right;
        T minimum;
        if (tmp) {
            while (tmp->m_left)
                tmp = tmp->m_left;
            minimum = tmp->m_data;
        }
        if (prev->m_left == node) {
            delete_elem(tmp->m_data);
            if (tmp) prev->m_left = new tree_elem<T>(minimum, node->m_left, node->m_right);
            else {prev->m_left = NULL;}
            delete[] node;
            size--;
            return;
        }
        if (prev->m_right == node) {
            delete_elem(tmp->m_data);
            if (tmp) {prev->m_right = new tree_elem<T>(minimum, node->m_left, node->m_right);}
            else {prev->m_right = NULL;}
            delete[] node;
            size--;
            return;
        }
    }


    bool is_equals_nodes(tree_elem<T>* first,tree_elem<T>* second){
        if (!first || !second){
            return false;
        }

        if(!second->m_right && !second->m_left && first->m_data == second->m_data){
            return true;
        }

        if(second->m_left && !first->m_left){
            return false;
        }

        if(second->m_right && !first->m_right)
            return false;

        if(first->m_data != second->m_data)
            return false;

        if(first->m_right && second->m_right && !second->m_left)
            return is_equals_nodes(first->m_right,second->m_right);

        if(first->m_left && second->m_left && !second->m_right)
            return is_equals_nodes(first->m_left,second->m_left);

        if(first->m_left && second->m_left && first->m_data == second->m_data)
            return is_equals_nodes(first->m_left,second->m_left);

        if(first->m_right && second->m_right && first->m_data == second->m_data)
            return is_equals_nodes(first->m_right,second->m_right);

        return 0;
    }

    bool is_subtree(binary_tree<T>* first,binary_tree<T>* second){
        tree_elem<T>* node = first->m_root;
        while(node && node->m_data != second->m_root->m_data){
            if(node->m_data > second->m_root->m_data)
                node = node->m_left;
            else
                node = node->m_right;
        }

        return is_equals_nodes(node,second->m_root);
    }

    void Threaded(){
        thread(m_root, NULL, NULL);
    }



    void Map(void(*func)(tree_elem<T>*)){
        tree_elem<T>* node = m_root;
        map_node(func,node);
    }

    void Where(bool(*func)(tree_elem<T>*)){
        tree_elem<T>* node = m_root;
        where_node(func,node);
    }

    T Reduce(T(*func)(tree_elem<T>*,T)){
        tree_elem<T>* node = m_root;
        T node_sum = 0;
        Reduce_help(func,node,node_sum);
        return node_sum;
    }

    std::string tree_to_string(){

        tree_elem<T>* node = m_root;
        std::vector<std::string> result;
        tree_to_string_help(node,result);

        std::string result_str = "";
        for (int i = 0; i < result.size(); ++i) {
            result_str += result[i] + " ";
        }
        return result_str;
    }

    std::vector<T> tree_to_vector(){
        tree_elem<T>* node = m_root;
        std::vector<T> result;
        tree_to_vector_help(node, result);
        return result;
    }

    void tree_to_sring(int var){
        tree_elem<T>* node = m_root;
        std::vector<std::string> result;
        switch (var) {
            case 1:{
                tree_to_string_help_KLP(node, result);
                break;}
            case 2:{
                tree_to_string_help_KPL(node, result);
                break;}
            case 3:{
                tree_to_string_help_LKP(node, result);
                break;}
            case 4:{
                tree_to_string_help_PKL(node, result);
                break;}
            case 5:{
                tree_to_string_help_LPK(node, result);
                break;}
            case 6:{
                tree_to_string_help_PLK(node, result);
                break;}
        }

        for (int i=0; i<result.size(); i++){
            std::cout << result[i] << "\t";
        }
        std::cout << std::endl;
    }
};